from pyexpat import model
from tkinter.font import names
from unicodedata import category, name
from django.db import models

from model_utils.models import TimeStampedModel, SoftDeletableModel



class Author(TimeStampedModel, SoftDeletableModel):
	'''Modelo de autores'''
	id = models.AutoField(primary_key=True)
	full_name = models.CharField(max_length=50, null=False, blank=False, unique=True)
	
	def __str__(self) -> str:
		return self.full_name

class Category(TimeStampedModel, SoftDeletableModel):
	'''Modelo de categorias'''
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=50, null=False, blank=False, unique=True)
	
	def __str__(self) -> str:
		return self.name

class Book(TimeStampedModel, SoftDeletableModel):
	'''	Modelo de libros'''
	id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=50, null=False, blank=False, unique=True)
	sub_title = models.CharField(max_length=50, null=True, blank=True)
	author = models.ManyToManyField(Author, verbose_name='Autor', blank=False)
	category = models.ManyToManyField(Category, verbose_name='Categoria', blank=True)
	date_published = models.CharField(max_length=60, null=False, blank=False, verbose_name="Fecha de publicacion")
	editor = models.TextField(max_length=50, null=True, blank=True)
	description = models.TextField(blank=True, null=True)
	image = models.ImageField(blank=True, null=True)
	fount = models.CharField(max_length=20, blank=False,null=False, default='Local_DB')
	
	def __str__(self) ->str:
		return self.title

