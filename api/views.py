from R5_library.settings import API_GOOGLE

#Django_REST_FRAMEWORK
from rest_framework.response import Response
from rest_framework import viewsets, mixins
from rest_framework import status

# Python
import requests
from datetime import datetime

# Models
from .models import Book, Category, Author

# Serializers
from .Serializers.BookSerializer import BookSerializers, AuthorSerializers, CategorySerializers

class Booksviewset(viewsets.ModelViewSet):
    '''
    Api view del modelo de Books
    '''
    queryset = Book.objects.all()
    serializer_class = BookSerializers
    filterset_fields = ('id','title','author__full_name','category__name')
    search_fields = ('id','title','author__full_name','category__name')
    ordering_fields = ('id','title','author__full_name','category__name')

    def get_serializer(self, *args, **kwargs):
        '''
        Sobre escribo metodo de serializer para
        buscar en la api de google en caso de
        no tener resultados en la bd local
        '''
        if self.request.method == 'GET':
            query = super().get_serializer(*args, **kwargs)
            if len(query.data) == 0:
                search_string = ''
                for _,value in self.request.GET.items():
                    search_string += (value + '+')
                ans = requests.get(API_GOOGLE +'?q='+ search_string)
                response_value = ans.json() 
                response_value['fount'] = 'Google_Books'
                return Response(response_value, status=status.HTTP_200_OK)
        
        if self.request.method == 'POST' and self.request.data.get('fount') == 'Google_Books':
            ans = requests.get(API_GOOGLE+'/'+ self.request.data.get('id'))

            print('KWARGS: ', kwargs)
            print('')
            book_json = {}
            book_json['title'] = ans.json().get('volumeInfo').get('title')
            book_json['description'] = ans.json().get('volumeInfo').get('description')
            book_json['date_published'] = ans.json().get('volumeInfo').get('publishedDate')

            # Search author and category and create if not exist
            authors_pk = []
            for aut in ans.json().get('volumeInfo').get('authors'):
                search_author = Author.objects.filter(full_name=aut).values('id')
                if len(search_author) == 0:
                    new_author = AuthorSerializers(data={'full_name': aut})
                    if new_author.is_valid():
                        new_author.save()
                    search_author = Author.objects.filter(full_name=aut).values('id')
                    authors_pk.append(search_author[0]['id'])
                else:
                    authors_pk.append(search_author[0]['id'])
            book_json['author'] = authors_pk
            

            categories_pk = []
            request_categories = ans.json().get('volumeInfo').get('categories')
            print('CATEGORIAS: ', request_categories)
            if request_categories:
                for cat in request_categories:
                    search_category = Category.objects.filter(name=cat).values('id')
                    if len(search_category) == 0:
                        new_category = CategorySerializers(data={'name': cat})
                        if new_category.is_valid():
                            new_category.save()
                        search_category = Category.objects.filter(name=cat).values('id')
                        categories_pk.append(search_category[0]['id'])
                    else:
                        categories_pk.append(search_category[0]['id'])
            
            book_json['category'] = categories_pk

            print('json_book==> ', book_json)

            new_book = BookSerializers(data=book_json)
            return new_book
        
        return super().get_serializer(*args, **kwargs)


class Authorsviewset(viewsets.ModelViewSet):
    '''
    Api view del modelo de Author
    '''
    queryset = Author.objects.all()
    serializer_class = AuthorSerializers
    search_fields = ('id','full_name')
    ordering_fields = ('id','full_name')

class Categoriesviewset(viewsets.ModelViewSet):
    '''
    Api view del modelo de Category
    '''
    queryset = Category.objects.all()
    serializer_class = CategorySerializers
    search_fields = ('id','name')
    ordering_fields = ('id','name')



